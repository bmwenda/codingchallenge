import React from 'react';

const CompletedTasks = ({ tasks }) => {
  const formattedDate = (dateTime) => {
    const date = new Date(dateTime);
    const dateOptions = { hour: '2-digit', minute: '2-digit' };
    return date.toLocaleTimeString('en-US', dateOptions);
  };

  return (
    <>
      {
        tasks && tasks.map((task) => (
          <div className="row" key={task.id}>
            <div className="col d-flex task-item">
              <img src={task.avatar} alt={task.description} className="img-fluid rounded-circle" />
              <p>{task.description}</p>
              <p className="ml-auto">{formattedDate(task.updated_at)}</p>
            </div>
          </div>
        ))
      }
    </>
  );
};

export default CompletedTasks;
