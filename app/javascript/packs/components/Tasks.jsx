import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import CompletedTasks from './CompletedTasks';
import UnCompletedTasks from './UnCompletedTasks';

const Tasks = () => {
  const fetchTasks = async () => {
    const url = '/tasks.json';
    const response = await fetch(url);
    if (response.ok) {
      const tasks = await response.json();
      return tasks;
    }
    throw new Error(`GET ${url} failed with status ${response.status}`);
  };

  const updateTask = async (taskId, taskParams) => {
    const url = `/tasks/${taskId}`;
    const response = await fetch(url, taskParams);
    if (response.ok) {
      const task = await response.json();
      return task;
    }
    throw new Error(`PUT ${url} failed with status ${response.status}`);
  };

  const [tasks, setTasks] = useState([]);
  useEffect(() => {
    fetchTasks()
      .then((response) => {
        setTasks(response);
      })
      .catch(() => { throw new Error('An error occurred while fetching tasks'); });
  }, []);

  const handleClickTask = (taskId) => {
    const params = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ completed: true }),
    };

    updateTask(taskId, params)
      .then(() => {
        fetchTasks()
          .then((response) => {
            setTasks(response);
          });
      })
      .catch(() => { throw new Error('An error occurred while updating tasks'); });
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col col-lg-6 d-flex header">
          <h2 className="mr-auto">Tasks</h2>
          <Link to="/new-task" className="ml-auto" data-toggle="modal" data-target="#task-add-modal">+</Link>
        </div>
      </div>
      <div className="row">
        <div className="col col-lg-6 tasks-section">
          <CompletedTasks tasks={tasks.completed} />
          <UnCompletedTasks tasks={tasks.uncompleted} onClickTask={handleClickTask} />
        </div>
      </div>
    </div>
  );
};

export default Tasks;
