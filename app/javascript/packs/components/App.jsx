import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Tasks from './Tasks';
import NewTask from './NewTask';

const App = () => (
  <Switch>
    <Route exact path="/" component={Tasks} />
    <Route exact path="/new-task" component={NewTask} />
  </Switch>
);

export default App;
