import React from 'react';

const UnCompletedTasks = ({ tasks, onClickTask }) => (
  <>
    {
        tasks && tasks.map((task) => (
          <div className="row" key={task.id}>
            <div className="col d-flex task-item">
              <img src={task.avatar} alt={task.description} className="img-fluid rounded-circle" />
              <p>{task.description}</p>
              <input type="checkbox" id={`completed_${task.id}`} className="ml-auto" onClick={() => onClickTask(task.id)} />
            </div>
          </div>
        ))
      }
  </>
);

export default UnCompletedTasks;
