import React from 'react';
import $ from 'jquery';

const NewTask = (props) => {
  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.target);
    const task = {
      description: data.get('description'),
      avatar: data.get('avatar'),
    };
    const params = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(task),
    };

    fetch('/tasks', params)
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Creating task failed with status ${response.status}`);
        }
      });

    $('#task-add-modal').modal('hide');
    props.history.push('/');
  };

  return (
    <div className="container">
      <div className="modal fade" id="task-add-modal" tabIndex="-1" role="dialog" aria-labelledby="task-add-modal-Label" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <div className="container-fluid">
                <div className="row">
                  <div className="col">
                    <div className="header">
                      <h2>Add Task</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-body">
              <div className="container-fluid">
                <div className="row">
                  <div className="col">
                    <form className="task-form" onSubmit={handleSubmit}>
                      <div className="form-group">
                        <label htmlFor="description">
                          Task Description
                          <input type="text" id="description" className="form-control" name="description" required />
                        </label>
                      </div>
                      <div className="form-group">
                        <label htmlFor="avatar">
                          Avatar URL
                          <input type="text" className="form-control" id="avatar" name="avatar" required />
                        </label>
                      </div>
                      <div className="d-flex justify-content-center">
                        <input type="submit" className="btn btn-primary mx-auto" value="Add" />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewTask;
