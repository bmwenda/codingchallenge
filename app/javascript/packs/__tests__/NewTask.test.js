import React from 'react';
import { render } from '@testing-library/react';
import NewTask from '../components/NewTask';

describe('New task component', () => {
  it('Renders task form', () => {
    const { queryByLabelText } = render(
      <NewTask />,
    );

    expect(queryByLabelText(/Task Description/)).toBeTruthy();
    expect(queryByLabelText(/Avatar URL/)).toBeTruthy();
  });
});
