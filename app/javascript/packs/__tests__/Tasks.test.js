import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Tasks from '../components/Tasks';

describe('Tasks component', () => {
  it('Renders successfully', () => {
    const div = document.createElement('div');
    ReactDom.render(
      <BrowserRouter>
        <Tasks />
      </BrowserRouter>,
      div,
    );
    ReactDom.unmountComponentAtNode(div);
  });
});
