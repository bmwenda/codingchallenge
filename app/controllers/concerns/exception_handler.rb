module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound, with: :not_found
    rescue_from ActionController::ParameterMissing, with: :bad_request
  end

  def not_found(error)
    render json: { message: { error: error.message } }, status: :not_found
  end

  def bad_request(error)
    render json: { message: { error: error.message } }, status: :bad_request
  end
end
