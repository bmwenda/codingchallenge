class TasksController < ApplicationController
  def index
    respond_to do |format|
      format.html
      format.json { render json: { completed: Task.completed, uncompleted: Task.uncompleted } }
    end
  end

  def create
    task = Task.new(task_params)

    if task.save
      render json: task, status: :created
    else
      error_message = {
        errors: task.errors.full_messages,
        type: "Task Create Error"
      }
      render json: { message: error_message }, status: :bad_request
    end
  end

  def show
    task = Task.find(params[:id])
    render json: task
  end

  def update
    task = Task.find(params[:id])

    if task.update(completed: task_params[:completed])
      render json: task, status: :ok
    else
      error_message = {
        errors: task.errors.full_messages,
        type: "Task Update Error"
      }
      render json: { message: error_message }, status: :bad_request
    end
  end

  private

  def task_params
    params.require(:task).permit(:id, :description, :avatar, :completed)
  end
end
