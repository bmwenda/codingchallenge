class Task < ApplicationRecord
  validates :description, :avatar, presence: true

  scope :completed, -> { where(completed: true).order(updated_at: :desc) }
  scope :uncompleted, -> { where(completed: false).order(updated_at: :desc) }
end
