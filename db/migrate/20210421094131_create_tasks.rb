class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.text :description
      t.string :avatar
      t.boolean :completed, null: false, default: false

      t.timestamps
    end
  end
end
