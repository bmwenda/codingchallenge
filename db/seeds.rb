tasks = [
  {
    description: "Single line description text",
    avatar: "https://i.pravatar.cc/150?img=33"
  },
  {
    description: "Lorem ipsum dolor sit amet consectetur adipiscing elit",
    avatar: "https://i.pravatar.cc/150?img=16"
  },
  {
    description: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris",
    avatar: "https://i.pravatar.cc/150?img=51"
  },
  {
    description: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim",
    avatar: "https://i.pravatar.cc/150?img=62"
  },
  {
    description: "Et harum quidem rerum facilis est et expedita distinctio",
    avatar: "https://i.pravatar.cc/150?img=61"
  }
]

tasks.each do |task_attributes|
  Task.create!(task_attributes)
end
