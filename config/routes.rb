Rails.application.routes.draw do
  resources :tasks, only: [:index, :show, :create, :update]
  root to: "tasks#index"
  match "*path", to: "tasks#index", via: :all
end
