# DoubleGDP Coding Challenge

## Introduction
This project is part of an exercise for DoubleGDP's interview process. It is a simple web application that allows users to create Tasks and update them after completion. The project is a rails backend with a react frontend.

## Table of contents
- [Installation](#installation)
- [Database setup](#database-setup)
- [Start server](#start-server)
- [Testing](#testing)
- [Linting](#linting)
- [Improvements](#improvements)

## Installation
You will need the following dependencies installed in your machine.
### Dependencies
1. [Ruby 3.0.0](https://www.ruby-lang.org/en/downloads/)
2. [Rails  6.1.3](https://weblog.rubyonrails.org/releases/)
3. [Node 14.15.0](https://nodejs.org/en/)
4. [Yarn](https://yarnpkg.com/getting-started/install)

For ruby, it is recommended to use a version manager such as [RVM](https://rvm.io/) or [rbenv](https://github.com/rbenv/rbenv). After successfully installing ruby with your preferred version manager, install rails 6 with the following command:

```
    gem install rails -v 6.1.3
```

To set up Nodejs, it is recommended to use [node version manager(nvm)](https://nodejs.org/en/download/package-manager/). Follow the instructions in the documentation to set up in your local environment.

### Installation Steps
Clone the repo and `cd` into the folder `codingchallenge`. You will now need to set up the backend and frontend dependencies.

Backend:
```
    bundle install
```

Frontend:
```
    yarn install
```

Check to confirm all dependencies were installed without any errors.

## Database setup
In this step, create the development database, run migrations and add seed data. To do this, run the following commands sequentially.

```
    rails db:create
    rails db:migrate
    rails db:seed
```
### Start server
Start the application by running `rails server`.
Navigate to the the url `localhost: 3000` on your browser and you should see the application up and running.

## Testing
Run the following commands to run tests.

Backend tests:
```
    rspec spec
```

Frontend tests:
```
    yarn jest app/javascript/packs
```

## Linting
Inspect any linting errors or warnings by running the following commands.

Backend:
```
    rubocop
```

Frontend:
```
    yarn run eslint --ext .jsx app/javascript/packs
```

## Improvements
In any project, there is always room for improvement and this project is no different. In my case, the below areas would be of importance:

1. Frontend tests - My knowledge in testing react components and UI testing best practices is currently limited. I will need to read extensively about this topic and add more robust UI tests.
2. TasksList page - has a lot of areas to improve on, key among them making the description text more appealing for small screens. It would also be great to handle pagination for ease of navigation.
