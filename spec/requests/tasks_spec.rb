require "rails_helper"

RSpec.describe "Tasks", type: :request do
  describe "GET /tasks" do
    it "Fetches tasks" do
      get tasks_path, headers: { "ACCEPT" => "application/json" }

      expect(response).to have_http_status(200)
    end
  end

  describe "POST /tasks", type: :request do
    context "when params are valid" do
      it "returns a 201 status code" do
        task_attributes = {
          task: {
            description: "A task",
            avatar: "http://example.com"
          }
        }
        post tasks_path, params: task_attributes, headers: { "ACCEPT" => "application/json" }

        expect(response).to have_http_status(201)
      end
    end

    context "when params are not valid" do
      it "returns a 400 status code" do
        task_attributes = {
          task: {
            avatar: "http://example.com"
          }
        }
        post tasks_path, params: task_attributes, headers: { "ACCEPT" => "application/json" }

        expect(response).to have_http_status(400)
      end
    end
  end

  describe "PUT /tasks/:id" do
    before do
      @task = Task.create(description: "A task", avatar: "http://example.com")
    end

    context "when params are valid" do
      it "returns a 200 status code" do
        task_attributes = {
          task: { completed: true }
        }
        put task_path(@task.id), params: task_attributes, headers: { "ACCEPT" => "application/json" }
        @task.reload

        expect(response).to have_http_status(200)
        expect(@task.completed).to be_truthy
      end
    end

    context "when params are not valid" do
      it "returns a 400 status code" do
        task_attributes = {
          task: {}
        }
        put task_path(@task.id), params: task_attributes, headers: { "ACCEPT" => "application/json" }

        expect(response).to have_http_status(400)
      end

      it "returns a 404 status code" do
        task_attributes = {
          task: { completed: true }
        }
        put task_path(100), params: task_attributes, headers: { "ACCEPT" => "application/json" }

        expect(response).to have_http_status(404)
      end
    end
  end
end
